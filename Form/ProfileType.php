<?php

namespace SoftPixel\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Expression;

class ProfileType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('nome')
                ->add('email')
                 ->add('avatarFile', 'vich_image', array(
                    'required' => false,
                    'allow_delete' => true,
                    'download_link' => true,
                    'label' => false
                ))
                ->add('enviar', 'submit', array('label' => 'Salvar', 'attr' => array('class' => 'btn btn-primary')))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'SoftPixel\AdminBundle\Entity\User',
            'csrf_protection' => false 
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'profile';
    }

}
