<?php

namespace SoftPixel\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use SoftPixel\AdminBundle\Form\RolePermissionType;

class RoleType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('role')
                ->add('name')
                ->add('permissions', 'collection', array(
                    'type' => new RolePermissionType(),
                    'label' => 'Permissões'
                ))
                ->add('enviar','submit')
        ;
        
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'SoftPixel\AdminBundle\Entity\Role'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'adminbundle_role';
    }

}
