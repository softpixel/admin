<?php

namespace SoftPixel\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Expression;
use SoftPixel\AdminBundle\Form\SiteType;

class RegisterType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->setAttributes(array('class' => 'form account-form'))
                ->add('nome', null, array('label' => false, 'attr' => array('placeholder' => 'Nome Completo')))
                ->add('email', null, array('label' => false,  'attr' => array('placeholder' => 'E-mail')))
                ->add('password', 'repeated', array(
                    'type' => 'password',
                    'invalid_message' => 'Os campos de senha deve ser iguais',
                    'options' => array('attr' => array('class' => 'password-field')),
                    'required' => false,
                    'first_options' => array('label' => false, 'constraints' => array(
                            new NotBlank(array('message' => 'Campo de senha em branco')),
                            new Length(array('min' => 6, 'minMessage' => 'Campo de senha deve conter no mínimo 6 caracteres'))
                        ),
                        'attr' => array('class' => 'form-control', 'placeholder' => 'Digite sua nova senha'),
                        'required' => true),
                    'second_options' => array('label' => false, 'constraints' => array(
                            new NotBlank(array('message' => 'Campo de senha em branco')),
                            new Length(array('min' => 6, 'minMessage' => 'Campo de senha deve conter no mínimo 6 caracteres'))
                        ),
                        'attr' => array('class' => 'form-control', 'placeholder' => 'Digite novamente a senha'),
                        'required' => true),
                ))
                ->add('enviar', 'submit', array('label' => ' Criar conta', 'attr' => array('class' => 'btn btn-secondary btn-block btn-lg')))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'SoftPixel\AdminBundle\Entity\User',
            'csrf_protection' => true
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'user';
    }

}
