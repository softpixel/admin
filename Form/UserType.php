<?php

namespace SoftPixel\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Expression;
use SoftPixel\AdminBundle\Form\SiteType;

class UserType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nome', null, array('label' => 'Nome'))
            ->add('email', 'email', array('label' => 'E-mail'))
            ->add('password', 'repeated', array(
                'type' => 'password',
                'invalid_message' => 'Os campos de senha deve ser iguais',
                'options' => array('attr' => array('class' => 'password-field')),
                'required' => false,
                'first_options' => array('label' => 'Senha', 'constraints' => array(
                    new NotBlank(array('message' => 'Campo de senha em branco')),
                    new Length(array('min' => 6, 'minMessage' => 'Campo de senha deve conter no mínimo 6 caracteres'))
                ),
                    'attr' => array('class' => 'form-control', 'placeholder' => 'Digite sua senha'),
                    'required' => true),
                'second_options' => array('label' => 'Confirmação', 'constraints' => array(
                    new NotBlank(array('message' => 'Campo de senha em branco')),
                    new Length(array('min' => 6, 'minMessage' => 'Campo de senha deve conter no mínimo 6 caracteres'))
                ),
                    'attr' => array('class' => 'form-control', 'placeholder' => 'Digite novamente a senha'),
                    'required' => true),
            ))
            ->add('isActive', null, array('label' => 'Está ativo?'))
            ->add('roles', 'entity', array(
                'class' => 'SoftPixelAdminBundle:Role',
                'property' => 'name',
                'multiple' => true,
                'expanded' => true,
                'required' => true,
                'label' => 'Papéis'
            ))
            ->add('enviar', 'submit', array('label' => 'Salvar', 'attr' => array('class' => 'btn btn-primary')));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SoftPixel\AdminBundle\Entity\User',
            'csrf_protection' => true
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'user';
    }

}
