SoftPixel AdminBundle
=====================

Instalação
----------

No composer json:
	* require: "softpixel/adminbundle": "dev-master"
	* repositories: "repositories": [
        {
            "type": "vcs",
            "url": "https://lipeassis@bitbucket.org/softpixel/admin.git"
        }
    ],

No AppKernel:
	* new Symfony\Bundle\AsseticBundle\AsseticBundle(),
    * new Stof\DoctrineExtensionsBundle\StofDoctrineExtensionsBundle(),
    * new Liip\ImagineBundle\LiipImagineBundle(),
    * new Knp\Bundle\PaginatorBundle\KnpPaginatorBundle(),
    * new Vich\UploaderBundle\VichUploaderBundle(),
    * new SoftPixel\AdminBundle\SoftPixelAdminBundle(),

No config.yml
    *- { resource: @SoftPixelAdminBundle/Resources/config/config.yml }

No routing.yml
    * soft_pixel_admin:
    *    resource: "@SoftPixelAdminBundle/Controller/"
    *    type:     annotation

 No security.yml

     security:
         acl:
             connection: default
         encoders:
             SoftPixel\AdminBundle\Entity\User:
                 algorithm: md5
                 encode_as_base64: false
                 iterations: 0

         role_hierarchy:
             ROLE_ADMIN:       ROLE_USER
             ROLE_SUPER_ADMIN: [ROLE_ADMIN, ROLE_ALLOWED_TO_SWITCH]

         providers:
             our_db_provider:
                 entity:
                     class: SoftPixelAdminBundle:User
                     property: email

         firewalls:
             main:
                 pattern:    ^/
                 http_basic: ~
                 anonymous: ~
                 form_login:
                     login_path: /login
                     check_path: /login_check
                     #logout: /logout
                     use_referer: true
                 logout:
                     path:   /logout
                     target: /
                 remember_me:
                     key:      "%secret%"
                     lifetime: 604800 # 1 week in seconds
                     path:     /
                         # ...


         access_control:
         - { path: ^/login, roles: IS_AUTHENTICATED_ANONYMOUSLY }
         - { path: ^/register, roles: IS_AUTHENTICATED_ANONYMOUSLY }
         - { path: ^/, roles: [ROLE_USER,ROLE_ADMIN,ROLE_TESTE] }

 No console:
    * php app/console doctrine:schema:create
    * php app/console asset:install
    * php app/console asset:dump