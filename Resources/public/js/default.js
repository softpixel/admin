
function initMasks() {
    $('.mask_date').mask('00/00/0000', {placeholder: "00/00/0000",selectOnFocus: true});
    $('.mask_time').mask('00:00:00', {placeholder: "00:00:00",selectOnFocus: true});
    $('.mask_date_time').mask('00/00/0000 00:00:00', {placeholder: "00/00/0000 00:00:00",selectOnFocus: true});
    $('.mask_inss').mask('00.000.00000/00', {placeholder: "00.000.00000/00",selectOnFocus: true});
    $('.mask_pis').mask('000.00000.00-0', {placeholder: "000.00000.00-0",selectOnFocus: true});
    $('.mask_cep').mask('00000-000', {placeholder: "00000-000",selectOnFocus: true});
    $('.mask_phone').mask('0000-0000');
    $('.mask_phone_with_ddd').mask('(00) 0000-0000');
    $('.mask_cellphone_with_ddd').mask('(00) 00000-0000');
    $('.phone_us').mask('(000) 000-0000');
    $('.mixed').mask('AAA 000-S0S');
    $('.mask_cpf').mask('000.000.000-00', {reverse: true,placeholder: "000.000.000-00",selectOnFocus: true});
    $('.mask_cnpj').mask('000.000.000/0000-00', {reverse: true,placeholder: "000.000.000/0000-00",selectOnFocus: true});
    $('.mask_money').mask('000.000.000.000.000,00', {reverse: true,placeholder: "0,00",selectOnFocus: true});
    var SPMaskBehavior = function (val) {
            return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
        },
        spOptions = {
            onKeyPress: function (val, e, field, options) {
                field.mask(SPMaskBehavior.apply({}, arguments), options);
            },selectOnFocus: true
        };

    $('.mask_telefone').mask(SPMaskBehavior, spOptions);

    $(".select2").select2({theme: 'bootstrap'});
    $('.datepicker').datepicker ({
        autoclose: true,
        todayHighlight: true,
        language: "pt-BR",
        format: "dd/mm/yyyy",
    });
    $('.timepicker').timepicker ();
}
$(document).ready(function () {

    $('#deleteActionModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var url = button.data('url') // Extract info from data-* attributes
        var nome = button.data('nome') // Extract info from data-* attributes
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this)
        modal.find('.confirm-button').attr('href', url)
        modal.find('.delete-name').text(nome)
    });

    initMasks();

    $('.navbar-notification').on('show.bs.dropdown', function () {
        var countNotificacoes = parseInt($("#count-notificacoes").text());
        if (countNotificacoes > 0) {
            $.getJSON(BASE_URL + '/notificacoes/mark-read',function(data) {
                if (data == 'OK') {
                    $("#count-notificacoes").text('0');
                }
            });
        }

    })
});