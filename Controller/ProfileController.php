<?php

namespace SoftPixel\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use SoftPixel\AdminBundle\Form\ChangePasswordType;
use SoftPixel\AdminBundle\Form\ProfileType;

/**
     * @Route("/profile")
     */

class ProfileController extends Controller {

    /**
     * @Template()
     * @Route("/", name="admin_profile")
     */
    public function indexAction(Request $request) {

        $user = $this->getUser();
        $form = $this->createForm(new ProfileType(), $user);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $user = $form->getData();
            $userRepo = $this->getDoctrine()->getRepository('SoftPixelAdminBundle:User');
            $userRepo->editar($user);
            $this->addFlash('success', 'Seu perfil foi alterado com sucesso!');
            return $this->redirectToRoute('admin_profile');
        }
        return array(
            'form' => $form->createView(),
            'user' => $user
        );
    }

    /**
     * @Template()
     * @Route("/profile/change-password", name="admin_profile_change_password")
     */
    public function changePasswordAction(Request $request) {

        $user = $this->getUser();
        $user->setPassword(null);

        $form = $this->createForm(new ChangePasswordType(), $user);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $user = $form->getData();
            $userRepo = $this->getDoctrine()->getRepository('SoftPixelAdminBundle:User');

            $hash = $this->get('security.password_encoder')->encodePassword($user, $user->getPassword());

            $user->setPassword($hash);

            $userRepo->editar($user);
            $this->addFlash('success', 'A sua senha foi alterada com sucesso!');
            return $this->redirectToRoute('admin_profile_change_password');
        }
        return array(
            'form' => $form->createView(),
            'user' => $user
        );
    }

    

}
