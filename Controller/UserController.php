<?php

namespace SoftPixel\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use SoftPixel\AdminBundle\Form\UserType;
use SoftPixel\AdminBundle\Entity\User;

/**
 * @Route("/usuarios")
 */
class UserController extends Controller {

    /**
     * @Route("/", name="_admin_user_index")
     */
    public function indexAction(Request $request) {

        $itens_por_pagina = 10;
        $options = $request->get('filter',array());

        $userRepo = $this->getDoctrine()->getRepository('SoftPixelAdminBundle:User');
        $query = $userRepo->getUsers($options);

        $paginator = $this->get('knp_paginator');
        $users = $paginator->paginate(
                $query, $request->query->getInt('page', 1), $itens_por_pagina
        );


        return $this->render(
                        'SoftPixelAdminBundle:User:index.html.twig', array('pagination' => $users)
        );
    }

    /**
     * @Route("/adicionar", name="_admin_user_adicionar")
     */
    
    public function adicionarAction(Request $request) {

        $user = new User();
        $form = $this->createForm(new UserType(), $user);

        $form->handleRequest($request);
        
        if ($form->isValid()) {
            $user = $form->getData();
            $user->setUsername(uniqid());
            $user->setPassword(md5($user->getPassword()));
            $userRepo = $this->getDoctrine()->getRepository('SoftPixelAdminBundle:User');
            $userRepo->adicionar($user);
            $this->addFlash('success','Usuário adicionado com sucesso!');
            return $this->redirectToRoute('_admin_user_index');
        }
        
        return $this->render(
                        'SoftPixelAdminBundle:User:adicionar.html.twig', array('form' => $form->createView())
        );
    }

    /**
     * @Route("/editar/{user}", name="_admin_user_editar")
     */

    public function editarAction(Request $request, User $user) {

        $form = $this->createForm(new UserType(), $user);
        $form->remove('password');
        $form->handleRequest($request);

        if ($form->isValid()) {
            $user = $form->getData();
            $userRepo = $this->getDoctrine()->getRepository('SoftPixelAdminBundle:User');
            $userRepo->editar($user);
            $this->addFlash('success','Usuário editado com sucesso!');
            return $this->redirectToRoute('_admin_user_index');
        }

        return $this->render(
            'SoftPixelAdminBundle:User:editar.html.twig', array('form' => $form->createView())
        );
    }
    /**
     * @Route("/status/{user}", name="_admin_user_status")
     */

    public function statusAction(Request $request, User $user) {

        $userRepo = $this->getDoctrine()->getRepository('SoftPixelAdminBundle:User');
        $user->setIsActive( !$user->getIsActive() );
        $userRepo->editar($user);
        $this->addFlash('success','Usuário editado com sucesso!');
        return $this->redirectToRoute('_admin_user_index');
    }
}
