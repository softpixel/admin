<?php

namespace SoftPixel\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use SoftPixel\AdminBundle\Entity\Notificacao;
use Symfony\Component\HttpFoundation\JsonResponse;
/**
 * @Route("/notificacoes")
 */
class NotificacaoController extends Controller {

    /**
     * @Route("/", name="_notificacoes")
     */
    public function indexAction(Request $request) {
        $notificacaoService = $this->get('notificacao.service');
        $notificacaoService->markAsRead();
        $query = $notificacaoService->findAll();
        $paginator = $this->get('knp_paginator');
        $notificacoes = $paginator->paginate(
                $query, 1, 50
        );
        
        return $this->render(
                        'SoftPixelAdminBundle:Notificacoes:index.html.twig', array('notificacoes' => $notificacoes)
        );
    }
    
    /**
     * @Route("/mark-read", name="_notificacoes_mark_read")
     */
    public function markReadAction(Request $request) {

        $this->get('notificacao.service')->markAsRead();
        return new JsonResponse('OK');
    }

}
