<?php

namespace SoftPixel\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use SoftPixel\AdminBundle\Form\ChangePasswordType;
use SoftPixel\AdminBundle\Form\RegisterType;
use SoftPixel\AdminBundle\Entity\User;

class SecurityController extends Controller
{

    /**
     * @Route("/login", name="admin_security_login")
     */
    public function loginAction(Request $request)
    {
        $authenticationUtils = $this->get('security.authentication_utils');

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render(
            'SoftPixelAdminBundle:Security:login.html.twig', array(
                'last_username' => $lastUsername,
                'error' => $error,
            )
        );
    }

    /**
     * @Route("/login_check", name="admin_security_login_check")
     */
    public function loginCheckAction()
    {
        // this controller will not be executed,
        // as the route is handled by the Security system
    }

    /**
     * @Route("/logout", name="admin_security_logout")
     */
    public function logoutsAction()
    {
        // this controller will not be executed,
        // as the route is handled by the Security system
    }

    /**
     * @Template()
     * @Route("/login/reset-password", name="admin_profile_reset_password")
     */
    public function resetPasswordAction(Request $request)
    {

        if ($request->isMethod('POST')) {

            $email = $request->get('_email');
            $userRepo = $this->getDoctrine()->
            getRepository('SoftPixelAdminBundle:User');
            $user = $userRepo->findOneBy(array('email' => $email));

            if ($user) {
                $token = md5(uniqid() . $user->getEmail());
                $user->setToken($token);
                $userRepo->editar($user);

                $emailService = $this->get('admin.email.service');
                $corpoEmail = $this->renderView(
                    'SoftPixelAdminBundle:_email:resetPassword.html.twig', array('user' => $user));
                $emailService->AddMessage($user->getEmail(), 'Instruções para resetar sua senha', $corpoEmail);


                $this->addFlash('success', 'Você receberá em breve um e-mail com as instruções para alteração de sua senha.');
                return $this->redirectToRoute('admin_profile_reset_password');
            }
            $this->addFlash('error', 'O e-mail digitado não foi encontrado em nossa base de dados');
        }

        return array();
    }

    /**
     * @Template()
     * @Route("/login/create-password/{token}", name="admin_profile_create_password")
     */
    public function createNewPasswordAction(Request $request, $token)
    {
        $userRepo = $this->getDoctrine()->
        getRepository('SoftPixelAdminBundle:User');
        $user = $userRepo->findOneBy(array('token' => $token));

        if (!$user) {
            return new Response('Token não encontrado', 404);
        }

        $form = $this->createForm(new ChangePasswordType(), $user);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $hash = $this->get('security.password_encoder')
                ->encodePassword($user, $user->getPassword());

            $user->setPassword($hash);
            $user->setToken(null);

            $userRepo->editar($user);
            $this->addFlash('success', 'Sua senha foi resetada com sucesso!');
            return $this->redirectToRoute('_admin');
        }

        return array('form' => $form->createView());
    }

    /**
     * @Template()
     * @Route("/register", name="admin_register")
     */
    public function registerAction(Request $request)
    {

        $userRepo = $this->getDoctrine()->getRepository('SoftPixelAdminBundle:User');

        $roleRepo = $this->getDoctrine()->getRepository('SoftPixelAdminBundle:Role');


        $adminService = $this->get('admin.service');
        $adminService->generateRules();

        if (!$this->getParameter('register_form') && $adminService->countUsers() > 0) {
            return $this->redirectToRoute('admin_security_login');
        }

        $defaultRole = $this->getParameter('role_default');
        $users = $userRepo->findAll();
        if (count($users) == 0) {
            $defaultRole = 'ROLE_SUPER_ADMIN';
        }
        $roles = $roleRepo->findBy(array('role' => $defaultRole));

        $user = new User();
        $user->setRoles($roles);

        $form = $this->createForm(new RegisterType(), $user);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $uniqd = strtolower(substr($user->getNome(), 0, 1) . uniqid());
            $token = md5($uniqd);
            $user->setUsername($uniqd);
            $user->setPassword(md5($user->getPassword()));
            $user->setIsActive(false);
            $user->setToken($token);

            $user->setRoles($roles);
            $userRepo->adicionar($user);

            $message = \Swift_Message::newInstance()
                ->setSubject('Ative seu cadastro no MResult')
                ->setFrom('contato@mresult.com.br')
                ->setTo($user->getEmail())
                ->setBody(
                    $this->renderView(
                        'SoftPixelAdminBundle:Security:emailRegister.html.twig', array('user' => $user)
                    ), 'text/html'
                );
            $this->get('mailer')->send($message);

            //$this->addFlash('success', 'Seu cadastro foi criado com sucesso');
            return $this->redirectToRoute('admin_register_sucess');
        }

        return array('form' => $form->createView());
    }

    /**
     * @Template()
     * @Route("/register/success", name="admin_register_sucess")
     */
    public function registerSuccessAction(Request $request)
    {

        return array();
    }

    /**
     * @Template()
     * @Route("/register/activate/{token}", name="admin_register_activate")
     */
    public function activateAction(Request $request, $token)
    {
        $userRepo = $this->getDoctrine()->
        getRepository('SoftPixelAdminBundle:User');

        $user = $userRepo->findOneBy(array('token' => $token));

        if (!$user) {
            $this->addFlash('error', 'Usuário inválido');
        } else {
            $user->setIsActive(true);
            $user->setToken('');
            $userRepo->update($user);
            $this->addFlash('success', 'Usuário ativado com sucesso!');
        }

        return $this->redirectToRoute('admin_security_login');
    }

}
