<?php

// src/AppBundle/Validator/Constraints/ContainsAlphanumeric.php
namespace SoftPixel\AdminBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ConstraintCnpj extends Constraint
{
    public $message = 'O Cnpj informado "%string%" é inválido.';

    public function validatedBy()
    {
        return get_class($this).'Validator';
    }
}