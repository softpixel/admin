<?php

namespace SoftPixel\AdminBundle\Repository;

use Doctrine\ORM\EntityRepository;
use SoftPixel\AdminBundle\Entity\Notificacao;

class NotificacaoRepository extends EntityRepository {

    public function getNotificacoes(array $options = array()) {
                
        $q = $this->createQueryBuilder('e')
                ->leftJoin('e.usuario', 'u')
                ->where('1=1');
        if (isset($options['usuario'])) {
            $q = $q->andWhere('u.id = :usuario')
                    ->setParameter('usuario', $options['usuario']->getId());
        }
        if (isset($options['dataLeitura'])) {
            
            if ($options['dataLeitura'] == 'null') {
                $q = $q->andWhere('e.dataLeitura is null');
            } else {
                $q = $q->andWhere('e.dataLeitura = :dataLeitura')
                        ->setParameter('dataLeitura', $options['dataLeitura']);
            }
        }
        $q = $q->orderBy('e.dataCadastro','DESC');
        return $q->getQuery();
    }

    public function adicionar(Notificacao $notificacao) {
        $this->_em->persist($notificacao);
        $this->_em->flush();
    }

    public function atualizar(Notificacao $notificacao) {
        $this->_em->merge($notificacao);
        $this->_em->flush();
    }

    public function excluir(Notificacao $notificacao) {
        $this->_em->remove($notificacao);
        $this->_em->flush();
    }

}
