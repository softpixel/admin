<?php

namespace SoftPixel\AdminBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;

class UserRepository extends  EntityRepository implements UserProviderInterface
{
    public function loadUserByUsername($username)
    {
        $user = $this->createQueryBuilder('u')
            ->where('u.username = :username OR u.email = :email')
            ->setParameter('username', $username)
            ->setParameter('email', $username)
            ->getQuery()
            ->getOneOrNullResult();

        if (null === $user) {
            $message = sprintf(
                'Unable to find an active admin AppBundle:User object identified by "%s".',
                $username
            );
            throw new UsernameNotFoundException($message);
        }

        return $user;
    }

    public function refreshUser(UserInterface $user)
    {
        $class = get_class($user);
        if (!$this->supportsClass($class)) {
            throw new UnsupportedUserException(
                sprintf(
                    'Instances of "%s" are not supported.',
                    $class
                )
            );
        }

        return $this->find($user->getId());
    }

    public function supportsClass($class)
    {
        return $this->getEntityName() === $class
            || is_subclass_of($class, $this->getEntityName());
    }
    
    
    public function adicionar(UserInterface $user) {
        $this->_em->persist($user);
        $this->_em->flush();
    }
    
    public function editar(UserInterface $user) {
        $this->_em->merge($user);
        $this->_em->flush();
    }
    
    public function getUsers(array $options = array()) {
        $q = $this->createQueryBuilder('u')
            ->leftJoin('u.roles','r');

        if (isset($options['keyword']) && strlen($options['keyword'])) {
            $q = $q->andWhere('u.nome like :nome')->setParameter('nome', '%' . $options['keyword'] . '%');
        }

        return $q->getQuery();
    }
}
