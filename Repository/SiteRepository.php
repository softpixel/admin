<?php

namespace SoftPixel\AdminBundle\Repository;

use Doctrine\ORM\EntityRepository;
use SoftPixel\AdminBundle\Entity\Site;

class SiteRepository extends  EntityRepository
{
   public function getSites(array $options = array()) {
        $q = $this->createQueryBuilder('u');
        return $q->getQuery();
    }
    
    public function adicionar(Site $site) {
        $this->_em->persist($site);
        $this->_em->flush();
    }

    public function atualizar(Site $site) {
        $this->_em->merge($site);
        $this->_em->flush();
    }

    public function excluir(Site $site) {
        $this->_em->remove($site);
        $this->_em->flush();
    }
}
