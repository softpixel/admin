<?php

namespace SoftPixel\AdminBundle\Repository;

use Doctrine\ORM\EntityRepository;
use SoftPixel\AdminBundle\Entity\Role;

class RoleRepository extends EntityRepository {

    public function getRoles() {
        $query = $this->createQueryBuilder('r')
                ->getQuery();
        return $query;
    }

    public function adicionar(Role $role) {
        $this->_em->persist($role);
        $this->_em->flush();
    }

    public function update(Role $role) {
        $this->_em->merge($role);
        $this->_em->flush();
    }

    public function delete(Role $role) {
        $this->_em->merge($role);
        $this->_em->flush();
    }

}
