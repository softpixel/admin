<?php

namespace SoftPixel\AdminBundle\Services;

use SoftPixel\AdminBundle\Entity\Role;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\SecurityContext;
use Coduo\PHPHumanizer\NumberHumanizer;
use Coduo\PHPHumanizer\DateTimeHumanizer;

class Admin
{

    protected $container;
    protected $doctrine;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->doctrine = $this->container->get('doctrine')->getEntityManager();
    }

    public function getBundles()
    {
        $bundles = $this->container->getParameter('kernel.bundles');
        return $bundles;
    }

    public function generateRules()
    {
        $roleRepostory = $this->doctrine->getRepository('SoftPixelAdminBundle:Role');

        $default_roles = array('ROLE_SUPER_ADMIN' => 'Super Admin', 'ROLE_ADMIN' => 'Admin', 'ROLE_USER' => 'Usuário');
        foreach ($default_roles as $roleId => $name) {
            $findrole = $roleRepostory->findOneBy(array('role' => $roleId));
            if (!$findrole) {
                $role = new Role();
                $role->setRole($roleId);
                $role->setName($name);
                $roleRepostory->adicionar($role);
            }
        }
    }
    public function countUsers()
    {
        $userRepostory = $this->doctrine->getRepository('SoftPixelAdminBundle:User');
        $users = $userRepostory->findAll();
        return count($users);
    }

    public function humanizeMetric($number) {
        return NumberHumanizer::metricSuffix($number,'pt_BR');
    }

    public function humanizeDateDiference(\DateTime $inicio, \DateTime $fim = null) {

        return NumberHumanizer::difference($fim,$inicio,'pt_BR');
    }
    public function humanizeDateToNow(\DateTime $inicio) {
        $fim = new \DateTime('now');
        return DateTimeHumanizer::difference($fim,$inicio,'pt_BR');
    }
}
