<?php

namespace SoftPixel\AdminBundle\Services;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\SecurityContext;
use SoftPixel\AdminBundle\Entity\Notificacao as NotificacaoEntity;

class Notificacao {

    protected $container;
    protected $doctrine;

    public function __construct(ContainerInterface $container) {
        $this->container = $container;
        $this->doctrine = $this->container->get('doctrine')->getEntityManager();
        
    }

    public function findNotRead() {
        $usuario = $this->container->get('security.context')->getToken()->getUser();
        
        $empresas = $this->doctrine->
                getRepository('SoftPixelAdminBundle:Notificacao')->
                getNotificacoes(array('usuario' => $usuario, 'dataLeitura' => 'null'))->getResult();
        return $empresas;
    }
    
     public function findAll() {
        $usuario = $this->container->get('security.context')->getToken()->getUser();
        
        $empresas = $this->doctrine->
                getRepository('SoftPixelAdminBundle:Notificacao')->
                getNotificacoes(array('usuario' => $usuario));
        return $empresas;
    }
    
    public function markAsRead() {
        $notifyRepo = $this->doctrine->getRepository('SoftPixelAdminBundle:Notificacao');
        $notificacoes = $this->findNotRead();
        $dataAtual = new \DateTime('now');
        foreach($notificacoes as $notificacao) {
            $notificacao->setDataLeitura($dataAtual);
            $notifyRepo->atualizar($notificacao);
        }
    }
    
    public function createNotification(NotificacaoEntity $notificacao) {
        $notifyRepo = $this->doctrine->getRepository('SoftPixelAdminBundle:Notificacao');
        $notifyRepo->adicionar($notificacao);
    }

}
