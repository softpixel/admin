<?php

namespace SoftPixel\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\Role\RoleInterface;
use Doctrine\Common\Collections\ArrayCollection;

/**
 *
 * @ORM\Table(name="sp_roles")
 * @ORM\Entity(repositoryClass="SoftPixel\AdminBundle\Repository\RoleRepository")
 * @ORM\HasLifecycleCallbacks() 
 */
class Role implements RoleInterface {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100, unique=true)
     */
    private $role;

    /**
     * @ORM\Column(type="string", length=250)
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity="User", mappedBy="roles")
     * */
    private $users;

    

    public function __construct() {
        $this->users = new ArrayCollection();
    }

    public function getId() {
        return $this->id;
    }

    public function getRole() {
        return $this->role;
    }

    public function getName() {
        return $this->name;
    }

    public function getUsers() {
        return $this->users;
    }

   

    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function setRole($role) {
        $this->role = $role;
        return $this;
    }

    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    public function setUsers($users) {
        $this->users = $users;
        return $this;
    }

   

}
