<?php

namespace SoftPixel\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\Role\RoleInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo; // gedmo annotations

/**
 *
 * @ORM\Table(name="sp_notificacoes")
 * @ORM\Entity(repositoryClass="SoftPixel\AdminBundle\Repository\NotificacaoRepository")
 * @ORM\HasLifecycleCallbacks() 
 */

class Notificacao {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="SoftPixel\AdminBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $usuario;

    /**
     * @var string
     *
     * @ORM\Column( type="datetime")
     */
    private $dataCadastro;

    /**
     * @var string
     *
     * @ORM\Column( type="datetime", nullable=true)
     */
    private $dataLeitura;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=250)
     */
    private $icone;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=250)
     */
    private $titulo;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=250)
     */
    private $descricao;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=250)
     */
    private $url;

    public function __construct() {
        $this->dataCadastro = new \DateTime('now');
        $this->setUrl('#');
    }

    public function getId() {
        return $this->id;
    }

    public function getUsuario() {
        return $this->usuario;
    }

    public function getDataCadastro() {
        return $this->dataCadastro;
    }

    public function getDataLeitura() {
        return $this->dataLeitura;
    }

    public function getIcone() {
        return $this->icone;
    }

    public function getTitulo() {
        return $this->titulo;
    }

    public function getDescricao() {
        return $this->descricao;
    }

    public function getUrl() {
        return $this->url;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function setUsuario($usuario) {
        $this->usuario = $usuario;
        return $this;
    }

    public function setDataCadastro($dataCadastro) {
        $this->dataCadastro = $dataCadastro;
        return $this;
    }

    public function setDataLeitura($dataLeitura) {
        $this->dataLeitura = $dataLeitura;
        return $this;
    }

    public function setIcone($icone) {
        $this->icone = $icone;
        return $this;
    }

    public function setTitulo($titulo) {
        $this->titulo = $titulo;
        return $this;
    }

    public function setDescricao($descricao) {
        $this->descricao = $descricao;
        return $this;
    }

    public function setUrl($url) {
        $this->url = $url;
        return $this;
    }

}
