<?php

namespace SoftPixel\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\HttpFoundation\File\File;
use Doctrine\Common\Collections\ArrayCollection;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\ExecutionContextInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * User
 *
 * @ORM\Table(name="sp_users")
 * @ORM\Entity(repositoryClass="SoftPixel\AdminBundle\Repository\UserRepository")
 * @UniqueEntity("email",message="Já existe cadastro com este e-mail" )
 * @ORM\HasLifecycleCallbacks
 * @Vich\Uploadable
 */
class User implements AdvancedUserInterface, \Serializable {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100, unique=true)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=250, unique=true)
     * @Assert\NotBlank(message="Campo E-mail em branco")
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=250)
     * @Assert\NotBlank(message="Campo Nome em branco")
     */
    private $nome;

    /**
     * @ORM\Column(type="string", length=50)
     * @Assert\NotBlank(message="Campo Senha em branco")
     */
    private $password;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     * @Assert\NotBlank(message="Campo Ativo em branco")
     */
    private $isActive;

    /**
     * @ORM\Column(type="string", length=50,nullable=true)
     */
    private $token;

    /**
     * @ORM\ManyToMany(targetEntity="Role", inversedBy="users")
     * @ORM\JoinTable(name="user_role")
     * */
    private $roles;

    public function getId() {
        return $this->id;
    }

    public function getUsername() {
        return $this->username;
    }

    public function getEmail() {
        return $this->email;
    }

    public function getNome() {
        return $this->nome;
    }

    public function getPassword() {
        return $this->password;
    }

    public function getIsActive() {
        return $this->isActive;
    }

    public function getToken() {
        return $this->token;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function setUsername($username) {
        $this->username = $username;
        return $this;
    }

    public function setEmail($email) {
        $this->email = $email;
        return $this;
    }

    public function setNome($nome) {
        $this->nome = $nome;
        return $this;
    }

    public function setPassword($password) {
        $this->password = $password;
        return $this;
    }

    public function setIsActive($isActive) {
        $this->isActive = $isActive;
        return $this;
    }

    public function setToken($token) {
        $this->token = $token;
        return $this;
    }

    public function serialize() {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            $this->nome
        ));
    }

    public function unserialize($serialized) {
        list (
                $this->id,
                $this->username,
                $this->password,
                $this->nome
                ) = unserialize($serialized);
    }

    public function eraseCredentials() {
        
    }

    public function getSalt() {
        
    }

    public function __construct() {
        $this->isActive = true;
        $this->roles = new ArrayCollection();
        $this->updatedAt = new \DateTime('now');
    }

    public function getRoles() {
        if (is_array($this->roles)) {
            return $this->roles;
        }
        return $this->roles->toArray();
    }

    public function setRoles($roles) {
        $this->roles = $roles;
        return $this;
    }

    public function isAccountNonExpired() {
        return true;
    }

    public function isAccountNonLocked() {
        return true;
    }

    public function isCredentialsNonExpired() {
        return true;
    }

    public function isEnabled() {
        return $this->isActive;
    }

    /**
     * @ORM\Column(type="datetime",nullable=true)
     *
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="string", length=100,nullable=true)
     */
    private $avatar;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * @Assert\Image(
     *     mimeTypes = "image/*",
     *     mimeTypesMessage = "Insira um arquivo do tipo IMAGEM"
     * ) 
     * @Vich\UploadableField(mapping="avatar", fileNameProperty="avatar")
     * 
     * @var File
     */
    private $avatarFile;

    public function getAvatar() {
        return $this->avatar;
    }

    public function getAvatarFile() {
        return $this->avatarFile;
    }

    public function setAvatar($avatar) {
        $this->avatar = $avatar;
        return $this;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     */
    public function setAvatarFile(File $avatarFile) {
        $this->avatarFile = $avatarFile;
        if ($this->avatarFile) {
            $this->updatedAt = new \DateTime('now');
        }
        return $this;
    }

    public function getUpdatedAt() {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTime $updatedAt) {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    /**
     * @Assert\Callback
     */
    public function validate(ExecutionContextInterface $context)
    {
        if (empty($this->getRoles())) {
            $context->addViolationAt(
                'roles',
                'Você precisa escolher um ou mais papéis',
                array(),
                null
            );
        }
    }
    
}
